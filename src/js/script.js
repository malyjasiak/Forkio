(function($) {
  "use strict";
  $(window).scroll(function() {
    if ($(document).scrollTop() > $(window).height() / 2) {
      $("#navbar-top").addClass("navbar-shrink");
      $('.collapse').collapse('hide');
    } else {
      $("#navbar-top").removeClass("navbar-shrink");
    }
  });
})(jQuery);

(function($) {
  $(window).resize(function() {
    $('.collapse').collapse('hide')
  });
})(jQuery);



(function($) {
  "use strict";
  var form = $("#subscriptionForm");
  var button = $("#submitSubscriptionButton");
  form.submit(function(e) {
    button.prop("disabled", true);
    e.preventDefault();
    $.post({
      url: "http://localhost:8080/subscriptions",
      data: form.serialize(),
      success: triggerSubscriptionSuccess(button),
      error: triggerSubscriptionFailure(button)
    });
  });
})(jQuery);

function triggerSubscriptionSuccess(button) {
  return function(data) {
    button.text("SUCCESS!");
    window.setInterval(setSubmitButtonDefault(button), 3000);
  }
}

function triggerSubscriptionFailure(button) {
  return function(err) {
    button.text("FAILURE!");
    window.setInterval(setSubmitButtonDefault(button), 3000);
  }
}

function setSubmitButtonDefault(button) {
  return function() {
    button.text("SUBSCRIBE");
    button.prop("disabled", false);
  }
}

// FETCH GITHUB FOLLOWERS
(function ($) {
  "use strict";
  const p = $('#github-followers');
  $.get({
    url: 'https://api.github.com/repos/Cajivah/Diffie-Helman-protocol-chat',
    // url: 'https://localhost',
    success: function(data) {
      p.text((data ? data.watchers_count : 'No') + ' Watchers');
    },
  });
})(jQuery);


//FETCH TWITTER FOLLOWERS
// TWITTER IS NOT SUPPORTING CORS!!!!!!!!!!!
(function ($) {
  "use strinct";
  const p = $('#twitter-followers');
  $.post({
    url: 'https://api.twitter.com/oauth2/token?grant_type=client_credentials',
    headers: {
      'Authorization': 'Basic ****CONFIDENTIAL****',
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    success: function(data) {
      $.get({
        url: 'https://api.twitter.com/1.1/followers/ids.json?screen_name=cajivah',
        headers: {
          'Authorization': 'Bearer ' + data.access_token,
        },
        success: function(data) {
          p.text(data.ids.length + ' Followers');
        },
      })
    },
  })
})(jQuery);

