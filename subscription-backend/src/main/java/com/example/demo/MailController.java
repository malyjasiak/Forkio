package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping("/subscriptions")
public class MailController {

     private final Set<String> subscribers = new HashSet<>();

     @PostMapping("")
     public void addSubscriber(@ModelAttribute final SubscriptionFormDTO subscriptionFormDTO) {
          subscribers.add(subscriptionFormDTO.getEmail());
     }

     @GetMapping("")
     public Set<String> getSubscribers() {
          return subscribers;
     }
}
