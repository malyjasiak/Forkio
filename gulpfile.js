var gulp = require("gulp");
var browserSync = require("browser-sync");
var sass = require("gulp-sass");
var sourcemaps = require("gulp-sourcemaps");
var autoprefixer = require("gulp-autoprefixer");
var cleanCSS = require("gulp-clean-css");
var uglify = require("gulp-uglify");
var concat = require("gulp-concat");
var imagemin = require("gulp-imagemin");
var changed = require("gulp-changed");
var htmlReplace = require("gulp-html-replace");
var htmlMin = require("gulp-htmlmin");
var del = require("del");
var sequence = require("run-sequence");
var svgmin = require("gulp-svgmin");
var pump = require("pump");
var babel = require("gulp-babel");

var paths = {
  out: "out/",
  src: "src/",
  cssin: "src/css/**/*.css",
  jsin: "src/js/**/*.js",
  imgin: "src/img/**/*.{jpg,jpeg,png,gif}",
  svgin: "src/img/**/*.svg",
  htmlin: "src/*.html",
  scssin: "src/scss/**/*.scss",
  cssout: "out/css/",
  jsout: "out/js/",
  imgout: "out/img/",
  svgout: "out/img/",
  htmlout: "out/",
  scssout: "src/css/",
  cssoutname: "main.css",
  jsoutname: "script.js",
  cssreplaceout: "css/main.css",
  jsreplaceout: "js/script.js"
};

gulp.task("reload", function() {
  browserSync.reload();
});

gulp.task("serve", ["sass"], function() {
  browserSync({
    server: paths.out
  });

  gulp.watch(paths.htmlin, ["html"]);
  gulp.watch(paths.jsin, ["js"]);
  gulp.watch(paths.scssin, ["sass"]);
  gulp.watch(paths.cssin, ["css"]);
  gulp.watch(paths.imgin, ["img"]);
  gulp.watch(paths.svgin, ["svg"]);
  gulp.watch([paths.htmlin, paths.jsin], ["reload"]);
});

gulp.task("sass", function() {
  return gulp
    .src(paths.scssin)
    .pipe(sourcemaps.init())
    .pipe(sass().on("error", sass.logError))
    .pipe(
      autoprefixer({
        browsers: ["last 3 versions"]
      })
    )
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(paths.scssout))
    .pipe(browserSync.stream());
});

gulp.task("css", function() {
  return gulp
    .src(paths.cssin)
    .pipe(concat(paths.cssoutname))
    .pipe(cleanCSS())
    .pipe(gulp.dest(paths.cssout));
});

gulp.task("js", function(cb) {
  pump([
    gulp.src(paths.jsin),
    babel({
      presets: ['es2015', 'stage-2'],
    }),
    uglify(),
    gulp.dest(paths.jsout)
  ], cb);
});

gulp.task("img", function() {
  return gulp
    .src(paths.imgin)
    .pipe(changed(paths.imgout))
    .pipe(imagemin([], {}))
    .pipe(gulp.dest(paths.imgout));
});

gulp.task("svg", function() {
  return gulp
    .src(paths.svgin)
    .pipe(svgmin())
    .pipe(changed(paths.svgout))
    .pipe(gulp.dest(paths.svgout));
});

gulp.task("html", function() {
  return gulp
    .src(paths.htmlin)
    .pipe(
      htmlReplace({
        css: paths.cssreplaceout,
        js: paths.jsreplaceout
      })
    )
    .pipe(
      htmlMin({
        sortAttributes: true,
        sortClassName: true,
        collapseWhitespace: true
      })
    )
    .pipe(gulp.dest(paths.out));
});

gulp.task("clean", function() {
  return del([paths.out]);
});

gulp.task("build", function() {
  sequence("clean","sass", ["html", "js", "css", "img", "svg"]);
});

gulp.task("default", sequence("build", "serve"));